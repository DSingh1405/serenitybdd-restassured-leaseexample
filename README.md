# Getting started with Serenity and Cucumber

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.

Serenity strongly encourages good test automation design, and supports several design patterns, including classic Page Objects, the newer Lean Page Objects/ Action Classes approach, and the more sophisticated and flexible Screenplay pattern.

The latest version of Serenity supports Cucumber 5.5.

## Lease Example - Serenity BDD
This is the testing project for RestAssured APIs. It evaluates the functionality of the search function using various goods. Developing positive and negative tests to test diverse combinations. Using the Serenity BDD framework with maven and a gitlab pipeline, CI/CD is achieved.

### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                        Test runners and supporting code 
      + starter                   Test Runner
      + step definition           Steps and Tests files
      + utils                     Common reusabile utilities
    + resources
      + features                  Feature files
        + search                  Feature file subdirectories
             search_by_keyword.feature
```

Serenity 3.2.5 introduced integration with WebdriverManager to download webdriver binaries.

## Search feature
The feature is expressed in the form of a scenario to facilitate its relationship with the business. It is described using Given, When, and Then. The scenario reaches the endpoint and verifies whether the received response corresponds to the product specified in the request.

```Gherkin
Feature: Search for the product

  When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he doesn not see the results
```

### The Step implementation
The code example on the main branch employs the Serenity pattern. The pattern defines tests by the jobs they accomplish. Instead of being represented as methods, tasks are objects. This makes them more versatile and composable, but also a bit more verbose.
Here is an example:
```java
    @When("he calls endpoint {string}")
	public void he_calls_endpoint(String arg0) {	
		baseURI = "https://waarkoop-server.herokuapp.com";
	}

    @Then("he sees the results displayed for apple")
	public void he_sees_the_results_displayed_for_apple() {

		given().when().get("api/v1/search/test/{product}", "apple")
		.then().statusCode(Constants.STATUS_CODE_200);	
	}
```
Serenity/Java classes emphasize reusable components and a highly legible declarative language, whereas Page Objects and Action Classes (seen below) opt for an imperative style. 

### The Action Classes implementation.

A more imperative-style implementation using the Action Classes pattern can be found in the `action-classes` branch. The glue code in this version looks this this:

```java
    	public WhenSearchingAppleDetails searchingProductAppleDetails;
	@When("he calls endpoint {string}")
	public void he_calls_endpoint(String arg0) {
		
		baseURI = "https://waarkoop-server.herokuapp.com";
	}		

	@Then("he sees the results displayed for apple")
	public void he_sees_the_results_displayed_for_apple() {

		given().when().get("api/v1/search/test/{product}", "apple")
		.then().statusCode(Constants.STATUS_CODE_200);
		
	}

	@Then("he sees the results displayed for mango")
	public void he_sees_the_results_displayed_for_mango() {

		given().when().get("api/v1/search/test/{product}", "mango")
		.then().statusCode(Constants.STATUS_CODE_200);
	}

	@Then("he doesn not see the results")
	public void he_doesn_dot_see_the_results() {
	
		given().when().get("api/v1/search/test/{product}", "car")
		.then().statusCode(Constants.STATUS_CODE_404);
	}
```
Each action class represents a distinct aspect of user behavior, such as navigating to a certain page, conducting a search, or retrieving search results. These classes are designed to be small and self-contained, making them more stable and manageable.

The `WhenSearchingAppleDetails` is an example of a very straightforward action class. In a larger application, it may include other methods pertaining to high-level data combination, but in our sample project, it only needs to test one positive and negative scenario for apple product:
```java
public class WhenSearchingAppleDetails{
	String product;
	
	@Before
	public void setBaseUrls() {
		
		baseURI = "https://waarkoop-server.herokuapp.com";
	}
	
	@Test
	  public void should_return_expected_content_type() {
	  
	  
	  given().when().get("api/v1/search/test/apple").then().contentType(ContentType.JSON)
	  .headers("Content-Type","application/json", "Connection", "keep-alive"); 
	  
	  Ensure.that("Headers for Apple Product", response -> response.headers("Content-Type", 
			  "application/json"));
	  
	}
```

This is accomplished with a regular Serenity Page Object. Page Objects are frequently extremely simple, keeping the base URI and testing various contexts.

```java
@Before
	public void setBaseUrls() {
		baseURI = "https://waarkoop-server.herokuapp.com";
	}
```

The additional classes  `WhenSearchingCarDetails`, `WhenSearchingDifferentProductDetails`and  `WhenSearchingMangoDetails` test the various product combinations. It must send the request and validate the answer in accordance with Serenity Rest Assured classes.

Although Serenity reduces the amount of boilerplate code you would normally have to write for a API test, the primary benefit of the technique employed in this example is not the number of lines of code produced. The true benefit is in the utilization of numerous tiny, stable courses, each of which focuses on a specific task. This implementation of the _Single Responsibility Principle_ significantly improves the test code's stability, readability, and maintainability.

## Executing the tests
To run the sample project, you can either just run the `TestRunner` test runner class, or run `mvn verify`

By default, the tests will run using Chrome. You can run them in Firefox by overriding the `driver` system property, e.g.
```json
$ mvn clean verify -Ddriver=firefox
```
Or
```json
$ gradle clean test -Pdriver=firefox
```
The test results will be recorded in the `target/site/serenity` directory.

## Generating the reports
Since the Serenity reports contain aggregate information about all of the tests, they are not generated after each individual test (as this would be extremenly inefficient). Rather, The Full Serenity reports are generated by the `serenity-maven-plugin`. You can trigger this by running `mvn serenity:aggregate` from the command line or from your IDE.

They reports are also integrated into the Maven build process: the following code in the `pom.xml` file causes the reports to be generated automatically once all the tests have completed when you run `mvn verify`?

```
             <plugin>
                <groupId>net.serenity-bdd.maven.plugins</groupId>
                <artifactId>serenity-maven-plugin</artifactId>
                <version>${serenity.maven.version}</version>
                <configuration>
                    <tags>${tags}</tags>
                </configuration>
                <executions>
                    <execution>
                        <id>serenity-reports</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>aggregate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
```

## Serenity Report Snapshot

![Snapshot of Serenity Report](/allureReports/serenityReport.png "Serenity Snapshot of Lease Example")
 
```

