package starter.stepdefinitions;

import org.junit.runner.RunWith;

import io.restassured.http.ContentType;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.Ensure;
import utils.Constants;

import static io.restassured.RestAssured.baseURI;
import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;

import org.junit.Before;
import org.junit.Test;

@RunWith(SerenityRunner.class)
public class WhenSearchingAppleDetails{
	String product;
	
	@Before
	public void setBaseUrls() {
		
		baseURI = "https://waarkoop-server.herokuapp.com";
	}
	
	@Test
	  public void should_return_expected_content_type() {
	  
	  
	  given().when().get("api/v1/search/test/apple").then().contentType(ContentType.JSON)
	  .headers("Content-Type","application/json", "Connection", "keep-alive"); 
	  
	  Ensure.that("Headers for Apple Product", response -> response.headers("Content-Type", 
			  "application/json"));
	  
	}
	 	
	@Test
	public void should_display_all_the_details_regarding_apple_product() {
		
		given().pathParam("products", "apple")
		.when()
		.get("api/v1/search/test/{products}")
		.then().body("title", everyItem(containsStringIgnoringCase("apple")));			
                
	}
	
	@Test
	public void should_get_error_for_adding_symbols_with_apple_product() {
		 
		given().pathParam("products", "apple12").
         when().get("api/v1/search/test/{products}")
         .then().statusCode(Constants.STATUS_CODE_404);
		
		Ensure.that("Not Found Message is displayed",  response -> response.body("detail.message",
                equalTo("Not found")));
	}
	
}
	
 
	
	

