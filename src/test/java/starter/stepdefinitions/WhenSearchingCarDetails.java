package starter.stepdefinitions;

import static io.restassured.RestAssured.baseURI;
import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import io.restassured.http.ContentType;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.Ensure;
import utils.Constants;

@RunWith(SerenityRunner.class)
public class WhenSearchingCarDetails {

String product;
	
	@Before
	public void setBaseUrls() {
		
		baseURI = "https://waarkoop-server.herokuapp.com";
	}
	
	@Test
	  public void should_return_expected_content_type() {
	  
	  
	  given().when().get("api/v1/search/test/car") .then().contentType(ContentType.JSON)
	  .headers("Content-Type","application/json"); 
	  
	}
	 		
	@Test
	public void should_get_error_for_adding_symbols_with_car_product() {
		 
		given().pathParam("products", "car").
         when().get("api/v1/search/test/{products}")
         .then().statusCode(Constants.STATUS_CODE_404);
		
		Ensure.that("Not Found Message is displayed",  response -> response.body("detail.message",
                equalTo("Not found")));
	}
}
