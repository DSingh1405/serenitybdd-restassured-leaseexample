package starter.stepdefinitions;

import static io.restassured.RestAssured.baseURI;
import static net.serenitybdd.rest.SerenityRest.given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.Constants;

public class SearchStepDefinitions {
	
	String product;
	
	public WhenSearchingAppleDetails searchingProductAppleDetails;
	@When("he calls endpoint {string}")
	public void he_calls_endpoint(String arg0) {
		
		baseURI = "https://waarkoop-server.herokuapp.com";
	}		

	@Then("he sees the results displayed for apple")
	public void he_sees_the_results_displayed_for_apple() {

		given().when().get("api/v1/search/test/{product}", "apple")
		.then().statusCode(Constants.STATUS_CODE_200);
		
	}

	@Then("he sees the results displayed for mango")
	public void he_sees_the_results_displayed_for_mango() {

		given().when().get("api/v1/search/test/{product}", "mango")
		.then().statusCode(Constants.STATUS_CODE_200);
	}

	@Then("he doesn not see the results")
	public void he_doesn_dot_see_the_results() {
	
		given().when().get("api/v1/search/test/{product}", "car")
		.then().statusCode(Constants.STATUS_CODE_404);
	}
}
