package starter.stepdefinitions;

import static io.restassured.RestAssured.baseURI;
import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.everyItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.restassured.http.ContentType;
import net.serenitybdd.junit.runners.SerenityRunner;


@RunWith(SerenityRunner.class)
public class WhenSearchingDifferentProductDetails {

String product;
	
	@Before
	public void setBaseUrls() {
		
		baseURI = "https://waarkoop-server.herokuapp.com";
	}
	
	@Test
	  public void should_return_expected_content_type() {  
	  
	  given().when().get("api/v1/search/test/tofu") .then().contentType(ContentType.JSON)
	  .headers("Content-Type","application/json"); 
	  
	}
	 	
	@Test
	public void should_display_all_the_details_regarding_tofu_product() {
		
		given().pathParam("products", "tofu")
		.when()
		.get("api/v1/search/test/{products}").then()
		.body("title", everyItem(containsStringIgnoringCase("tofu")));
			            
	}
	
	@Test
	public void should_display_all_the_details_regarding_water_product() {
		
		 given().when().get("api/v1/search/test/water") .then().contentType(ContentType.JSON)
		  .headers("Content-Type","application/json"); 
		
		 given().pathParam("products", "water")
		.when()
		.get("api/v1/search/test/{products}").then()
		.body("title", everyItem(containsStringIgnoringCase("water")));
			
                
	}
}
