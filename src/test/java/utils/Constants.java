package utils;

public interface Constants {

    /**
     * Constants for Response Type
     */
    	
    	public static int STATUS_CODE_200 = 200;
    	public static int STATUS_CODE_404 = 404;
    	public static int STATUS_CODE_401 = 401;

}
